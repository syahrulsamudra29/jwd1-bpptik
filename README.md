## APLIKASI KALKULATOR - TUGAS 4

<img alt="HTML5" src="https://img.shields.io/badge/html5%20-%23E34F26.svg?&style=for-the-badge&logo=html5&logoColor=white"/> 
<img alt="CSS3" src="https://img.shields.io/badge/css3%20-%231572B6.svg?&style=for-the-badge&logo=css3&logoColor=white"/>
<img alt="PHP" src="https://img.shields.io/badge/php-%23777BB4.svg?&style=for-the-badge&logo=php&logoColor=white"/>
<img alt="Bootstrap" src="https://img.shields.io/badge/bootstrap%20-%23563D7C.svg?&style=for-the-badge&logo=bootstrap&logoColor=white"/>

Program ini dibuat untuk melakukan operasi aritmatik sederhana yaitu penjumlahan, pengurangan, pembagian, dan perkalian dari 2 inputan yang ada. Dibangun dengan bahasa pemrograman PHP dan Boostrap untuk framework styling CSS nya. Untuk boostrap sudah di embed pada head index.php. Bootsrap dipanggil secara online melalui BootsrapCDN.

Berikut merupakan struktur folder projeknya : <br>
📦Tugas 4 <br>
 ┣ 📂assets <br>
 ┃ ┗ 📜style.css <br>
 ┣ 📜index.php <br>
 ┗ 📜README.md <br>

## Requirements

* XAMPP : PHP >= 7.3.9
* Google Chrome >= 93.0.4577.82

## Instalation

* XAMPP

   Download [XAMPP](https://www.apachefriends.org/download.html) sesuai OS (Operating System) masing-masing, kemudian install sesuai petunjuk.

## Usage

1. Letakan folder **Tugas 4** ke dalam 
    > *C:\xampp\htdocs*  (Windows).
    
5. Jalankan *XAMPP -> Apache*.
6. Buka Browser ketikan 
   > *localhost/Tugas4*.

## Credits
   Syahrul Samudra - BPPTIK Junior Web Developer Gel. 8