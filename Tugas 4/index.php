<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- link untuk memanggil bootstrap secara online -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- link untuk memanggil css eksternal -->
    <link rel="stylesheet" href="assets/style.css">
    <title>Tugas 4</title>
</head>

<body>
    <div class="kiri">
        <div class="ver">
            <form action="index.php" method="post">
                <!-- inisialisai form -->
                <label for="pertama" class="tebal">Masukan Bilangan ke-1 = </label>
                <input id="pertama" class="form-control" type="text" name="pertama"> <!-- input field dengan name pertama -->

                <label for="kedua" class="mt-2 tebal">Masukan Bilangan ke-2 = </label>
                <input id="kedua" type="text" class="form-control" name="kedua"> <!-- input field dengan name kedua -->

                <label for="kedua" class="mt-2 tebal">Masukan Operator</label>
                <select name="operator" class="form-control" id="operator">
                    <option value="tambah">+</option>
                    <option value="kurang">-</option>
                    <option value="bagi">/</option>
                    <option value="kali">*</option>
                </select>
                <br>
                <div class="">
                    <input name="btn" class="btn btn-primary mt-1" type="submit" value="Cari Hasil"> <!-- input dengan type submit yang digunakan untuk mengirim field angka -->
                </div>
            </form>
        </div>
    </div>
    <div class="kanan">
        <div class="ver-right">
            <?php
            if (isset($_POST['btn']) && isset($_POST['operator'])) { //ketika btn ditekan, dan membawa value dari operator maka
                $bil1 = $_POST['pertama']; //mengambil nilai text field bilangan pertama
                $bil2 = $_POST['kedua']; //mengambil nilai text field bilangan kedua
                $operasi = $_POST['operator']; //mengambil value option operator
                switch ($operasi) { //memasukan value dari option operator ke logika percabangan
                    case 'tambah': //jika valuenya 'tambah', maka
                        $hasil = $bil1 + $bil2; //akan menjumlahkan bil1 dan bil2 disimpan dalam var hasil
                        break;
                    case 'kurang': //jika valuenya 'kurang', maka
                        $hasil = $bil1 - $bil2; //akan mengurangi bil1 dan bil2 disimpan dalam var hasil
                        break;
                    case 'kali': //jika valuenya 'kali', maka
                        $hasil = $bil1 * $bil2; //akan mengalikan bil1 dan bil2 disimpan dalam var hasil
                        break;
                    case 'bagi': //jika valuenya 'bagi', maka
                        $hasil = $bil1 / $bil2; //akan membagi bil1 dan bil2 disimpan dalam var hasil
                        break;
                }
            }
            ?>
            <h1>Hasilnya = 
                <?php
                    if (isset($_POST['btn']) && isset($_POST['operator'])) { //ketika btn ditekan, dan membawa value dari operator maka
                ?>
                    <?= $hasil ?> <!-- menampikan variabel hasil -->
                <?php
                    }
                ?>
            </h1>
        </div>
    </div>
</body>

</html>