<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 3 - Syahrul Samudra</title>
</head>

<body>
    <form action="Tugas3_[SyahrulSamudra].php" method="post"> <!-- inisialisai form -->
        <label for="pertama">Masukan Bilangan ke-1 = </label> 
        <input id="pertama" type="number" name="pertama"> <!-- input field dengan name pertama -->
        <br>
        <label for="kedua">Masukan Bilangan ke-2 = </label>
        <input id="kedua" type="number" name="kedua"> <!-- input field dengan name kedua -->
        <br>
        <input name="btn" type="submit"></input> <!-- input dengan type submit yang digunakan untuk mengirim field angka -->
        <hr>
    </form>
    <br>
    <?php
    if (isset($_POST['btn'])) { //jika menerima inputan dari input btn maka,
        $satu = $_POST['pertama']; //mengambil nilai dari field pertama
        $kedua = $_POST['kedua']; //mengambil nilai dari field kedua

        //fungsi tambah, dengan parameter 2 inputan
        function tambah($x, $y)
        {
            $z = $x + $y; //variabel z akan menampung hasil penjumlahan dari 2 parameter inputan
            return "Hasil Penjumlahannya Adalah = $z <br>"; //echo variabel z
        }

        //fungsi pengurangan, dengan parameter 2 inputan
        function pengurangan($x, $y)
        {
            $z = $x - $y; //variabel z akan menampung hasil pengurangan dari 2 parameter inputan
            return "Hasil Pengurangannya Adalah = $z <br>"; //echo variabel z
        }

        //fungsi perkalian, dengan parameter 2 inputan
        function perkalian($x, $y)
        {
            $z = $x * $y; //variabel z akan menampung hasil perkalian dari 2 parameter inputan
            return "Hasil Perkaliannya Adalah = $z <br>"; //echo variabel z
        }

        //fungsi bagi, dengan parameter 2 inputan
        function bagi($x, $y)
        {
            $z = $x / $y; //variabel z akan menampung hasil pembagian dari 2 parameter inputan
            return "Hasil Pembagiannya Adalah = $z <br>"; //echo variabel z
        }

        echo tambah($satu, $kedua); //memanggil fungsi tambah, dengan inputan untuk mengisi parameter menggunakan $satu dan $dua
        echo pengurangan($satu, $kedua); //memanggil fungsi pengurangan, dengan inputan untuk mengisi parameter menggunakan $satu dan $dua
        echo perkalian($satu, $kedua); //memanggil fungsi perkalian, dengan inputan untuk mengisi parameter menggunakan $satu dan $dua
        echo bagi($satu, $kedua); //memanggil fungsi bagi, dengan inputan untuk mengisi parameter menggunakan $satu dan $dua
    }
    ?>
</body>

</html>