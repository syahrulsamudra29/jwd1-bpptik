<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 2 - Syahrul Samudra</title>
</head>

<body>
    <form action="Tugas2_[SyahrulSamudra].php" method="post"> <!-- inisialisai form -->
        <label for="masukan">Jumlah Bintang = </label>
        <input id="masukan" type="text" name="angka"> <!-- input field dengan name "angka" -->
        <br>
        <input type="submit" name="btn"></input> <!-- input dengan type submit yang digunakan untuk mengirim field angka -->
        <br>
    </form>

    <?php
    if (isset($_POST['btn'])) { //jika menerima inputan dari input btn maka,
        $angka = $_POST['angka']; //mengambil inputan dari field angka
        for ($i = $angka; $i > 0; $i--) { //perulangan untuk baris
            for ($j = $angka; $j >= $i; $j--) { //perulangan untuk kolom
                echo "*"; //menampilkan bintang
            }
            echo "<br>"; //untuk enter
        }
    }
    ?>
</body>

</html>