<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 - Syahrul Samudra</title>
</head>
<body>
<h1>========CETAK BILANGAN GANJIL DAN GENAP 1-100========</h1>
    <?php
        //membuat perulangan
        for ($i=1; $i < 101; $i++) { //inisialisai perulangan yang dimuali dari 0 hingga 100
            if ($i%2 != 0) { //logika ketika nilai perulangan dibagi 2 hasilnya tidak sama dengan 0 maka menampilkan ganjil
                echo "$i Adalah Bilangan Ganjil. <br>";
            }else{
                //selain itu akan menampilkan genap
                echo "$i Adalah Bilangan Genap. <br>";
            }
        }
    ?>
</body>
</html>