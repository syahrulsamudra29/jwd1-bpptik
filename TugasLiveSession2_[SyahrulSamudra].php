<html>
<head>
	<title>Tugas Live Session 2</title>
</head>
<body>
	<form method="post">
		<table>
			<tr>
				<td>
					Bilangan
				</td>
				<td>
					:
				</td>
				<td>
					<input type="number" name="bil"> <!-- text field untuk input jumlah index array -->
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" name="btn"> <!-- submit untuk mengirim jumlah index array yang ada pada field bil -->
				</td>
			</tr>
		</table>

		<br>

		<?php

		if (isset($_POST['btn'])) { //ketika btn ditekan maka,
			$bil = $_POST['bil']; //mengambil nilai bil

			//membuat sebuah form
			echo "<form method=\"post\">";
			for ($i = 0; $i < $bil; $i++) { //dimana, jumlah inputan pada form tergantung pada jumlah index array yang diinginkan, diambil dari bil
				echo "Angka - " . $i . " ";
				echo "<input type=\"number\" name=\"bil" . $i . "\" >"; //mamasukan nilai kepada setiap index array
				echo "<br>";
			}
			echo "<input type=\"submit\" name=\"btn2\">";
			echo "<input type=\"hidden\" name=\"hide_val\" value=\"" . $bil . "\">"; //membawa data bil ke proses selanjutnya
			echo "</form>";
		}

		if (isset($_POST['btn2'])) { //ketika btn2 di tekan maka
			$bil2 = $_POST['hide_val']; //mengambil nilai bil sebagai bil2
			$daftar_bil = array(); //membuat array dengan nama daftar_bil

			for ($i = 0; $i < $bil2; $i++) { //membuat perulangan untuk mengambil nilai lalu
				$str_idx = "bil" . $i;
				$daftar_bil[$i] = $_POST[$str_idx]; //memasukannya nilai tersebut dimasukan pada array daftar_bill
			}

			$jumlah_arr = array_sum($daftar_bil); //fungsi untuk menjumlahkan isi array dan disimpan dalam sebuah variabel
			$ratata = $jumlah_arr / $bil2; //mencari rata rata dengan membagi jumlah isi array dengan jumlah indexnya

			echo $ratata;
		}
		?>
	</form>
</body>
</html>